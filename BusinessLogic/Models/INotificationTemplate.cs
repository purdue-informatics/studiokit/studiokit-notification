﻿using StudioKit.Notification.Models.Interfaces;
using System;
using System.Collections.Generic;

namespace StudioKit.Notification.BusinessLogic.Models;

/// <summary>
/// Interface that represents a notification template that can be rendered. Normally a <see cref="NotificationTemplate{TTemplateData}"/>
/// will implement this interface in order to associate a template with a specific type of template data.
/// </summary>
/// <typeparam name="TTemplateData">The type of template data to be used for rendering.</typeparam>
/// <typeparam name="TGroup">The type of Group for TTemplateData</typeparam>
public interface INotificationTemplate<in TTemplateData, TGroup>
	where TTemplateData : BaseNotificationTemplateData<TGroup>
	where TGroup : IGroup
{
	/// <summary>
	/// Render the HTML and plain text versions of the template using the provided template data.
	/// </summary>
	/// <returns>A <see cref="NotificationMessage"/> with strings having been interpolated from the template data</returns>
	Func<TTemplateData, NotificationMessage> Render { get; }
}

/// <summary>
/// Interface that represents a notification that can be created and stored in a (covariant) collection like an
/// <see cref="IDictionary{TKey,TValue}"/>. Normally a <see cref="NotificationTemplate{TTemplateData}"/> will implement
/// this in order to be able to stored in collections along with otherwise invariant closed types of notification templates.
/// </summary>
public interface INotificationTemplate
{
	/// <summary>
	/// The activities that this template applies to.
	/// </summary>
	IEnumerable<string> Activities { get; }
}