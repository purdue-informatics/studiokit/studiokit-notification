﻿using System.Collections.Concurrent;
using System.Diagnostics;
using System.Linq;

namespace StudioKit.Notification.BusinessLogic.Models;

/// <summary>
/// Singleton for storing templates in an application. Templates are stored as <see cref="TargetedTemplates"/>, a pairing
/// of user and/or group templates per event
/// </summary>
public class NotificationTemplates

{
	public static NotificationTemplates GetInstance()
	{
		return Instance;
	}

	/// <summary>
	/// For the given event, add or replace a template for the target in the collection of templates. This method is thread safe
	/// </summary>
	/// <param name="notificationEvent"></param>
	/// <param name="template"></param>
	/// <param name="target">Who will be receiving the notification</param>
	public void AddOrReplaceTemplate(string notificationEvent, INotificationTemplate template, NotificationTarget target)
	{
		var templates = Templates.SingleOrDefault(t => t.Key.Equals(notificationEvent)).Value ?? new TargetedTemplates();
		if (target == NotificationTarget.User)
			templates.UserTemplate = template;
		else
			templates.GroupTemplate = template;
		Templates[notificationEvent] = templates;
	}

	public void AddOrReplaceTemplate(string notificationEvent, TargetedTemplates targetedTemplates)
	{
		Debug.Assert(targetedTemplates.UserTemplate != null || targetedTemplates.GroupTemplate != null,
			"targetedTemplates.UserTemplate != null || targetedTemplates.GroupTemplate != null");
		Templates[notificationEvent] = targetedTemplates;
	}

	/// <summary>
	/// Remove a template from the collection. This method is thread safe. Probably not useful outside
	/// of unit testing. Method is idempotent.
	/// </summary>
	/// <param name="notificationEvent"></param>
	public void RemoveTemplate(string notificationEvent)
	{
		Templates.TryRemove(notificationEvent, out _);
	}

	/// <summary>
	/// Find a template for a target by its event. Returns null if not found.
	/// </summary>
	/// <param name="notificationEvent"></param>
	/// <param name="target">Who will be receiving the notification</param>
	/// <returns></returns>
	public virtual INotificationTemplate FindTemplate(string notificationEvent, NotificationTarget target)
	{
		var targetedTemplates = Templates.SingleOrDefault(t => t.Key.Equals(notificationEvent)).Value;
		return target == NotificationTarget.User ? targetedTemplates?.UserTemplate : targetedTemplates?.GroupTemplate;
	}

	#region Private Implementation

	/// <summary>
	/// The dictionary holding the templates
	/// </summary>
	// ReSharper disable once StaticMemberInGenericType
	// This is by design so app instances can define user types
	private static readonly ConcurrentDictionary<string, TargetedTemplates> Templates =
		new ConcurrentDictionary<string, TargetedTemplates>();

	/// <summary>
	/// Private constructor per the Singleton pattern
	/// </summary>
	private NotificationTemplates()
	{
	}

	/// <summary>
	/// Private singleton instance
	/// </summary>
	private static readonly NotificationTemplates Instance = new NotificationTemplates();

	#endregion Private Implementation
}