﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace StudioKit.Notification.BusinessLogic.Models;

/// <summary>
/// Class that maintains definitions of all notification templates for an application and a mechanism
/// for installing them into the global notification template store. This is designed to be subclassed
/// with the subclass defining all the elements in the <see cref="EventTemplates"/> dictionary, one
/// per notification event as defined in the application.
/// </summary>
/// <example>
/// Note: because C# doesn't support static inheritance, the way you'd use this is, class is i.e.:
/// <code>
/// 		new ApplicationNotificationTemplateConfiguration().InstallAllTemplates(NotificationTemplates&lt;User&gt;.GetInstance());
///  </code>
/// </example>
public class BaseNotificationTemplateConfiguration
{
	/// <summary>
	/// Dictionary that holds all the templates used by the application. One set of templates (group, user, or both)
	/// should be specified for every event defined in the application's static class that defines notification events.
	/// Each application will need to define such a class a static string member for each event,
	/// (typically named ApplicationNotificationEvent)
	/// </summary>
	protected static Dictionary<string, TargetedTemplates>
		EventTemplates;

	private static readonly Assembly CallingAssembly = Assembly.GetCallingAssembly();

	/// <summary>
	/// Locates all assembly resources in the caller (subclass') assembly and assembles a dictionary
	/// of image attachments that can be used in notifications. Key is the name of the file, value is a
	/// stream that can be passed to, i.e. EmailClient for generating attachments.
	/// To use this, simply add image files to the project in which the subclass of this class is located.
	/// i.e. Create an "Images" folder in your project and use Visual Studio to include those images in the project.
	/// Set the "Build Action" in the file properties to "Embedded Resource". In the HTML of your template,
	/// if you wanted to include "Images\logo.png" in the HTML message, you would do as follows:
	/// <example>
	/// &lt;img width='210' src='cid:Images.logo.png' alt='Circuit' style='display:block; margin: 10px auto;' /&gt;
	/// </example>
	/// where the "src" tag uses the image from the project, replacing backslashes with periods
	/// </summary>
	protected static readonly Dictionary<string, Func<Stream>> ImageAttachments =
		CallingAssembly.GetManifestResourceNames()
			.Where(rn => new[] { "png", "gif", "jpg", "jpeg" }.Contains(rn.Substring(rn.LastIndexOf('.') + 1)))
			.ToDictionary<string, string, Func<Stream>>(
				imageName => imageName.Replace($"{CallingAssembly.GetName().Name}.", ""),
				imageName => () => CallingAssembly.GetManifestResourceStream(imageName));

	/// <summary>
	/// Filter all possible image attachments to just the ones needed for a certain message.
	/// </summary>
	/// <param name="imageNames">
	/// An enumerable of the names of the files
	/// <example>new[]{"Images.logo.png", "Images.header.png"}</example>
	/// </param>
	/// <returns></returns>
	protected static Dictionary<string, Func<Stream>> ImageAttachmentsNamed(IEnumerable<string> imageNames)
	{
		return ImageAttachments
			.Where(ia => imageNames.Contains(ia.Key))
			.ToDictionary(ia => ia.Key, ia => ia.Value);
	}

	/// <summary>
	/// Install all templates in the global notification template store
	/// </summary>
	/// <param name="templates">The application-defined global template store</param>
	public void InstallAllTemplates(NotificationTemplates templates)
	{
		foreach (var template in EventTemplates)
		{
			templates.AddOrReplaceTemplate(template.Key, template.Value);
		}
	}
}