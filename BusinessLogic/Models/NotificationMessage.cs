﻿using System;
using System.Collections.Generic;
using System.IO;

namespace StudioKit.Notification.BusinessLogic.Models;

/// <summary>
/// This class represents a set of potential messages to be sent to a user in various formats.
/// Properties are read-only to satisfy <see cref="GetHashCode"/> requirements.
/// </summary>
public class NotificationMessage
{
	public NotificationMessage(string plaintext = null, string html = null, string subject = null,
		Dictionary<string, Func<Stream>> imageAttachments = null)
	{
		Plaintext = plaintext;
		Html = html;
		Subject = subject;
		ImageAttachments = imageAttachments;
	}

	public string Plaintext { get; }

	public string Html { get; }

	public string Subject { get; }

	public Dictionary<string, Func<Stream>> ImageAttachments { get; }

	#region Equality Comparison

	protected bool Equals(NotificationMessage other)
	{
		return Plaintext == other.Plaintext && Html == other.Html && Subject == other.Subject &&
				Equals(ImageAttachments, other.ImageAttachments);
	}

	public override bool Equals(object obj)
	{
		if (ReferenceEquals(null, obj)) return false;
		if (ReferenceEquals(this, obj)) return true;
		if (obj.GetType() != GetType()) return false;
		return Equals((NotificationMessage)obj);
	}

	public override int GetHashCode()
	{
		unchecked
		{
			var hashCode = (Plaintext != null ? Plaintext.GetHashCode() : 0);
			hashCode = (hashCode * 397) ^ (Html != null ? Html.GetHashCode() : 0);
			hashCode = (hashCode * 397) ^ (Subject != null ? Subject.GetHashCode() : 0);
			hashCode = (hashCode * 397) ^ (ImageAttachments != null ? ImageAttachments.GetHashCode() : 0);
			return hashCode;
		}
	}

	#endregion Equality Comparison
}