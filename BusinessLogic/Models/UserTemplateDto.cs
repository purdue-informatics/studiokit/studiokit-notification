﻿using StudioKit.Data;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Notification.Models.Interfaces;

namespace StudioKit.Notification.BusinessLogic.Models;

public class UserTemplateDto<TTemplateData, TGroup>
	where TGroup : ModelBase, IGroup
	where TTemplateData : BaseNotificationTemplateData<TGroup>, new()

{
	public IUser User { get; set; }
	public bool DisableEmail { get; set; }
	public TTemplateData TemplateData { get; set; }
}