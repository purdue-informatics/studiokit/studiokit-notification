﻿namespace StudioKit.Notification.BusinessLogic.Models;

/// <summary>
/// Used for specifying the intended recipient(s) of a notification
/// </summary>
public enum NotificationTarget
{
	User,
	Group
}