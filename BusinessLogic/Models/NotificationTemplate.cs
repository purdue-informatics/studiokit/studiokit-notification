﻿using StudioKit.Notification.Models.Interfaces;
using System;
using System.Collections.Generic;

namespace StudioKit.Notification.BusinessLogic.Models;

/// <summary>
/// This class represents a notification that will be sent to a user. It includes initialization of the
/// notification templates via the constructor and a method to render a notification, given some template data.
/// Templates use standard C# syntax for interpolated strings, ie. "Hello {n.User.FirstName} {n.User.LastName}"
/// where n is the prefix used for any interpolated data.
/// <remarks>
/// Your application may want to subclass <see cref="BaseNotificationTemplateData"/> to extend the shape of the data that is
/// passed to the render method.
/// Your application will want to add/manage items in the <see cref="NotificationTemplates"/> collection (singleton)
/// using its' public interface.
/// </remarks>
/// </summary>
public class NotificationTemplate<TTemplateData, TGroup> : INotificationTemplate<TTemplateData, TGroup>, INotificationTemplate
	where TTemplateData : BaseNotificationTemplateData<TGroup>
	where TGroup : IGroup
{
	/// <summary>
	/// A template used for rendering a notification from an instance of <see cref="TTemplateData"/>
	/// Activities can be used to filter the collection of <see cref="NotificationTemplates"/>about which
	/// groups of users the template applies to.
	/// </summary>
	/// <param name="subject">The subject of the message, if applicable</param>
	/// <param name="render">A function that accepts a <see cref="TTemplateData"/> instance and returns a rendered template</param>
	/// <param name="activities">The activities held by users to which this message applies</param>
	public NotificationTemplate(Func<TTemplateData, NotificationMessage> render, IEnumerable<string> activities = null)
	{
		Render = render;
		Activities = activities;
	}

	/// <inheritdoc/>
	public Func<TTemplateData, NotificationMessage> Render { get; }

	/// <inheritdoc/>
	public IEnumerable<string> Activities { get; }
}