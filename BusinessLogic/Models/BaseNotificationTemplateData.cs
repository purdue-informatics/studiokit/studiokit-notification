﻿using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Notification.Models.Interfaces;

namespace StudioKit.Notification.BusinessLogic.Models;

/// <summary>
/// This class represents a set of data to be used for populating messages to be sent to users.
/// Applications may want to derive from this class to pass additional data for interpolation
/// into notification messages.
/// </summary>
public class BaseNotificationTemplateData<TGroup>
	where TGroup : IGroup
{
	public TGroup Group { get; set; }

	public string NotificationEvent { get; set; }

	public IUser User { get; set; }

	public string BaseUrl { get; set; }
}