﻿namespace StudioKit.Notification.BusinessLogic.Models;

/// <summary>
/// Pairing of templates per event, one for notifying a single user, one for the group.
/// Either may be null. One should not be null. Methods for adding templates to
/// <see cref="NotificationTemplates"/> will prevent both from being null.
/// </summary>
public class TargetedTemplates
{
	public INotificationTemplate UserTemplate { get; set; }
	public INotificationTemplate GroupTemplate { get; set; }
}