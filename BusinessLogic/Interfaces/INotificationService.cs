using StudioKit.Data;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Notification.BusinessLogic.Models;
using StudioKit.Notification.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Notification.BusinessLogic.Interfaces;

/// <summary>
/// Specifies methods to notify groups or individual users based on templates that have
/// been configured with replaceable parameters
/// </summary>
/// <typeparam name="TGroup">The type of group for associating with notifications</typeparam>
public interface INotificationService<TGroup>
	where TGroup : ModelBase, IGroup
{
	/// <summary>
	/// Notify a single user (subject to the user's activities and mail preferences)
	/// </summary>
	/// <param name="notificationEvent">The event that occurred requiring notification</param>
	/// <param name="user">The user to notify</param>
	/// <param name="principal">The principal requesting the notification be sent</param>
	/// <param name="templateTransformerAsync">
	/// An async function which accepts a <see cref="TTemplateData"/> instance and returns
	/// a <see cref="TTemplateData"/> instance. Subclasses of <see cref="BaseNotificationTemplateData{TGroup}"/> will specify additional properties needed
	/// for template population. This method gives the caller a per-template-data-instance function that can populate those additional
	/// properties with data in the caller's scope
	/// </param>
	/// <param name="groupId">(optional)The group context in which the event occurred</param>
	/// <param name="cancellationToken"></param>
	/// <typeparam name="TTemplateData">The type used to populate each of the notifications with data</typeparam>
	Task NotifyUserAsync<TTemplateData>(string notificationEvent, IUser user, IPrincipal principal, int? groupId,
		Func<TTemplateData, Task<TTemplateData>> templateTransformerAsync = null,
		CancellationToken cancellationToken = default)
		where TTemplateData : BaseNotificationTemplateData<TGroup>, new();

	/// <summary>
	/// Notify all users in a group (subject to each user's activities and mail preferences)
	/// </summary>
	/// <param name="groupId">The group context in which the event occurred</param>
	/// <param name="notificationEvent">The event that occurred requiring notification</param>
	/// <param name="principal">The principal requesting the notification be sent</param>
	/// <param name="templateTransformerAsync">
	/// An async function which accepts a <see cref="TTemplateData"/> instance and returns
	/// a <see cref="TTemplateData"/> instance. Subclasses of <see cref="BaseNotificationTemplateData{TGroup}"/> will specify additional properties needed
	/// for template population. This method gives the caller a per-template-data-instance function that can populate those additional
	/// properties with data in the caller's scope
	/// </param>
	/// <param name="userFilter">(Optional) Provide additional user filtering , e.g. assignment progress requirements.</param>
	/// <param name="cancellationToken"></param>
	/// <param name="shouldLog"></param>
	/// <typeparam name="TTemplateData">The type used to populate each of the notifications with data</typeparam>
	Task NotifyGroupAsync<TTemplateData>(int groupId, string notificationEvent, IPrincipal principal,
		Func<TTemplateData, Task<TTemplateData>> templateTransformerAsync = null, Func<IQueryable<IUser>,
			IQueryable<IUser>> userFilter = null, CancellationToken cancellationToken = default,
		bool shouldLog = true)
		where TTemplateData : BaseNotificationTemplateData<TGroup>, new();
}