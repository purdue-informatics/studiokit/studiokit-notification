using StudioKit.Notification.BusinessLogic.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StudioKit.Notification.BusinessLogic.Interfaces;

/// <summary>
/// Specifies a set of mechanisms for sending combinations of messages to combinations of users
/// </summary>
public interface INotificationTransport
{
	/// <summary>
	/// Send a message (optionally, a text and html alternative) to a list of destinations.
	/// </summary>
	/// <param name="notificationMessage">The message's body</param>
	/// <param name="destinations">The list of destinations to send the message to</param>
	Task SendAsync(NotificationMessage notificationMessage, IEnumerable<string> destinations);

	/// <summary>
	/// Send a message (optionally, a text and html alternative) to a destination.
	/// </summary>
	/// <param name="notificationMessage">The message's body</param>
	/// <param name="destination">The single destination to send the message to</param>
	Task SendAsync(NotificationMessage notificationMessage, string destination);

	/// <summary>
	/// Send a message (optionally, a text and html alternative) to a list of destinations
	/// with a specific body for each destination.
	/// </summary>
	/// <param name="addressedNotifications">
	/// A dictionary with the destination as the key
	/// and message body as the value
	/// </param>
	Task SendAsync(IDictionary<string, NotificationMessage> addressedNotifications);
}