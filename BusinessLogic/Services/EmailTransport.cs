using Azure;
using Azure.Communication.Email;
using Microsoft.Extensions.Logging;
using StudioKit.Configuration;
using StudioKit.Encryption;
using StudioKit.Notification.BusinessLogic.Interfaces;
using StudioKit.Notification.BusinessLogic.Models;
using StudioKit.Notification.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

[assembly: InternalsVisibleTo("StudioKit.Notification.Tests")]

namespace StudioKit.Notification.BusinessLogic.Services;

/// <summary>
/// <inheritdoc/>
/// Uses email as transport for sending messages to users
/// </summary>
public class EmailTransport : INotificationTransport
{
	#region Public Interface

	/// <summary>
	/// Constructor for this implementation of <see cref="INotificationTransport"/>
	/// </summary>
	/// <param name="emailClient">Azure client for sending emails</param>
	/// <param name="logger">Logger</param>
	public EmailTransport(EmailClient emailClient, ILogger<EmailTransport> logger)
	{
		_emailClient = emailClient;
		_logger = logger;
	}

	/// <summary>
	/// <inheritdoc/>
	/// Message will be sent as email. Uses the "fire and forget" method so we do not need to await
	/// the message having been sent. The lifecycle of an instance of this object should be
	/// application-scoped so the smtp client is not disposed of while the application is running.
	/// </summary>
	/// <param name="notificationMessage">The email's body</param>
	/// <param name="destinations">The list of email addresses to send the message to</param>
	public async Task SendAsync(NotificationMessage notificationMessage, IEnumerable<string> destinations)
	{
		var tier = EncryptedConfigurationManager.GetSetting(BaseAppSetting.Tier);
		var whitelistAddresses =
			EncryptedConfigurationManager.GetSetting(NotificationAppSetting.WhitelistEmailAddresses)
				.Split(',').Select(a => a.Trim());

		var mail = await CreateEmailMessageAsync(notificationMessage, destinations, tier, whitelistAddresses);
		if (mail != null)
		{
			await _emailClient.SendAsync(WaitUntil.Started, mail);
		}
	}

	/// <summary>
	/// <inheritdoc/>
	/// Message will be sent as email
	/// </summary>
	/// <param name="notificationMessage">The email's body</param>
	/// <param name="destination">The single email address to send the message to</param>
	public async Task SendAsync(NotificationMessage notificationMessage, string destination)
	{
		await SendAsync(notificationMessage, new List<string> { destination });
	}

	/// <summary>
	/// <inheritdoc/>
	/// Message will be sent as email
	/// </summary>
	/// <param name="addressedNotifications">
	/// A dictionary with the email address as the key
	/// and message body as the value
	/// </param>
	public async Task SendAsync(IDictionary<string, NotificationMessage> addressedNotifications)
	{
		foreach (var targetedNotification in addressedNotifications)
		{
			await SendAsync(targetedNotification.Value, new List<string> { targetedNotification.Key });
		}
	}

	#endregion Public Interface

	#region Private Implementation

	private readonly EmailClient _emailClient;
	private readonly ILogger<EmailTransport> _logger;

	/// <summary>
	/// Generate a <see cref="EmailMessage"/> to be used by an <see cref="EmailClient"/>
	/// </summary>
	/// <param name="notificationMessage">The email body, represented in both plaintext and HTML format</param>
	/// <param name="addresses">The email's recipient list</param>
	/// <param name="tier">The environment in which the message will be sent (for use with whitelisting)</param>
	/// <param name="whitelistAddresses">The permitted addresses in a non-production environment</param>
	/// <returns></returns>
	internal static async Task<EmailMessage> CreateEmailMessageAsync(NotificationMessage notificationMessage,
		IEnumerable<string> addresses, string tier = Tier.Prod, IEnumerable<string> whitelistAddresses = null)
	{
		if (string.IsNullOrWhiteSpace(notificationMessage.Html) && string.IsNullOrWhiteSpace(notificationMessage.Plaintext))
			throw new ArgumentException(Strings.BodyNotFound);

		//don't spam our users with test emails
		var addressesList = addresses.ToList();
		IEnumerable<string> filteredAddresses = addressesList;
		if (tier != Tier.Prod && whitelistAddresses != null)
		{
			filteredAddresses = addressesList.Intersect(whitelistAddresses).ToList();
		}

		if (!filteredAddresses.Any()) return null;

		var fromAddress = EncryptedConfigurationManager.GetSetting(NotificationAppSetting.FromAddress);
		var recipients = new EmailRecipients(filteredAddresses.Select(a => new EmailAddress(a)));
		var content = new EmailContent(notificationMessage.Subject)
		{
			PlainText = notificationMessage.Plaintext,
			Html = notificationMessage.Html
		};

		var emailMessage = new EmailMessage(fromAddress, recipients, content);

		if (notificationMessage.ImageAttachments == null) return emailMessage;

		foreach (var imageAttachment in notificationMessage.ImageAttachments)
		{
			var data = await BinaryData.FromStreamAsync(imageAttachment.Value());
			emailMessage.Attachments.Add(new EmailAttachment(imageAttachment.Key, MimeTypes.GetMimeType(imageAttachment.Key),
				data));
		}

		return emailMessage;
	}

	#endregion Private Implementation
}