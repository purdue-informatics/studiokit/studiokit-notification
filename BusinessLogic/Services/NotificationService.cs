﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using StudioKit.Data;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.ErrorHandling;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ErrorHandling.Interfaces;
using StudioKit.Notification.BusinessLogic.Interfaces;
using StudioKit.Notification.BusinessLogic.Models;
using StudioKit.Notification.DataAccess;
using StudioKit.Notification.Models;
using StudioKit.Notification.Models.Interfaces;
using StudioKit.Security.BusinessLogic.Models;
using StudioKit.Security.Properties;
using StudioKit.Sharding;
using StudioKit.Sharding.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using IUser = StudioKit.Data.Entity.Identity.Interfaces.IUser;

namespace StudioKit.Notification.BusinessLogic.Services;

/// <inheritdoc/>
/// <typeparam name="TNotificationLog">
/// The type used to store the data about the notification for persistence
/// </typeparam>
/// <typeparam name="TUser">The type of user for associating with notifications</typeparam>
/// <typeparam name="TGroup">The type of group for associating with notifications</typeparam>
/// <typeparam name="TRoleActivity">The type of RoleActivity</typeparam>
/// <typeparam name="TActivity">The type of Activity</typeparam>
/// <typeparam name="TGroupUserRole">The type of GroupUserRole</typeparam>
public class
	NotificationService<TNotificationLog, TUser, TGroup, TRoleActivity, TActivity,
		TGroupUserRole> : INotificationService<TGroup>
	where TNotificationLog : BaseNotificationLog<TGroup>
	where TUser : IdentityUser, IUser
	where TGroup : ModelBase, IGroup
	where TRoleActivity : class, IRoleActivity<TActivity>, new()
	where TActivity : class, IActivity, new()
	where TGroupUserRole : class, IGroupUserRole, new()

{
	private readonly NotificationDataAccess<TNotificationLog, TUser, TGroup, TRoleActivity, TActivity, TGroupUserRole> _dataAccess;

	private readonly Func<IUser, string> _destinationSelector;
	private readonly NotificationTemplates _notificationTemplates;
	private readonly IShardKeyProvider _shardKeyProvider;
	private readonly IErrorHandler _errorHandler;
	private readonly INotificationTransport _transport;

	/// <summary>
	/// Parameterless ctor for mocking
	/// </summary>
	public NotificationService()
	{
	}

	/// <summary>
	/// Constructor for <see cref="NotificationService{TNotificationLog, TUser, TGroup, TRoleActivity, TActivity, TGroupUserRole}"/>
	/// </summary>
	/// <param name="dataAccess">The utility function for data access for this class</param>
	/// <param name="transport">An instance that does the actual transmission of the notification</param>
	/// <param name="notificationTemplates">The singleton that is used to locate a template</param>
	/// <param name="destinationSelector">
	/// A function to select a destination address from <see cref="IUser"/>
	/// </param>
	/// <param name="shardKeyProvider">Provider to get shard key for accessing front-end url</param>
	/// <param name="errorHandler"></param>
	public NotificationService(
		NotificationDataAccess<TNotificationLog, TUser, TGroup, TRoleActivity, TActivity, TGroupUserRole> dataAccess,
		INotificationTransport transport,
		NotificationTemplates notificationTemplates,
		Func<IUser, string> destinationSelector,
		IShardKeyProvider shardKeyProvider,
		IErrorHandler errorHandler)
	{
		_dataAccess = dataAccess ?? throw new ArgumentNullException(nameof(dataAccess));
		_transport = transport ?? throw new ArgumentNullException(nameof(transport));
		_notificationTemplates = notificationTemplates ?? throw new ArgumentNullException(nameof(notificationTemplates));
		_destinationSelector = destinationSelector ?? throw new ArgumentNullException(nameof(destinationSelector));
		_shardKeyProvider = shardKeyProvider ?? throw new ArgumentNullException(nameof(shardKeyProvider));
		_errorHandler = errorHandler ?? throw new ArgumentNullException(nameof(errorHandler));
	}

	/// <inheritdoc/>
	public virtual async Task NotifyUserAsync<TTemplateData>(string notificationEvent, IUser user,
		IPrincipal principal, int? groupId,
		Func<TTemplateData, Task<TTemplateData>> templateTransformerAsync = null,
		CancellationToken cancellationToken = default)
		where TTemplateData : BaseNotificationTemplateData<TGroup>, new()
	{
		AssertSystemPrincipal(principal);
		try
		{
			var disableEmail = await _dataAccess.HasEmailDisabledAsync(user, cancellationToken);
			var template = GetTemplate(notificationEvent, NotificationTarget.User);
			TGroup group = null;
			if (groupId.HasValue)
				group = await _dataAccess.FindGroupAsync(groupId.Value, cancellationToken);

			await NotifyAsync(
				new List<UserTemplateDto<TTemplateData, TGroup>>
				{
					new()
					{
						User = user,
						DisableEmail = disableEmail
					}
				},
				notificationEvent,
				template,
				group,
				templateTransformerAsync,
				true,
				cancellationToken);
		}
		// We don't want any exception in notification to cause the calling method to fail
		catch (Exception e)
		{
			_errorHandler.CaptureException(e, new ExceptionUser { Id = user.Id, UserName = user.UserName, Email = user.Email });
		}
	}

	/// <inheritdoc/>
	public virtual async Task NotifyGroupAsync<TTemplateData>(int groupId, string notificationEvent,
		IPrincipal principal, Func<TTemplateData, Task<TTemplateData>> templateTransformerAsync = null,
		Func<IQueryable<IUser>, IQueryable<IUser>> userFilter = null, CancellationToken cancellationToken = default,
		bool shouldLog = true)
		where TTemplateData : BaseNotificationTemplateData<TGroup>, new()
	{
		AssertSystemPrincipal(principal);
		try
		{
			var template = GetTemplate(notificationEvent, NotificationTarget.Group);
			var group = await _dataAccess.FindGroupAsync(groupId, cancellationToken);
			var usersToNotifyQueryable = _dataAccess.GetUsersToNotifyQueryable<TTemplateData>(groupId, template, userFilter);
			var usersToNotify = await usersToNotifyQueryable.ToListAsync(cancellationToken);
			if (!usersToNotify.Any())
				return;
			await NotifyAsync(usersToNotify, notificationEvent, template, group, templateTransformerAsync, shouldLog, cancellationToken);
		}
		// We don't want any exception in notification to cause the calling method to fail
		catch (Exception e)
		{
			_errorHandler.CaptureException(e);
		}
	}

	#region PrivateImplementation

	/// <summary>
	/// Throw an exception if the principal is not SystemPrincipal
	/// </summary>
	/// <param name="principal"></param>
	/// <param name="caller">The method name of the caller</param>
	private void AssertSystemPrincipal(IPrincipal principal, [CallerMemberName] string caller = "")
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));
		if (!(principal is SystemPrincipal))
			throw new ForbiddenException(string.Format(Strings.RequiresSystemPrincipal, caller));
	}

	/// <summary>
	/// Get a template from the global collection
	/// </summary>
	/// <param name="notificationEvent">The event associated with the requested template</param>
	/// <param name="target">Who will be receiving the notification</param>
	/// <returns>The requested template</returns>
	private INotificationTemplate GetTemplate(string notificationEvent, NotificationTarget target)
	{
		var template = _notificationTemplates.FindTemplate(notificationEvent, target);
		if (template == null)
			throw new ApplicationException(string.Format(Properties.Strings.TemplateNotFound, notificationEvent));
		return template;
	}

	/// <summary>
	/// Perform user selection and mail preference logic. Send the notifications and log them.
	/// </summary>
	/// <param name="userTemplateDtos">The users to be notified</param>
	/// <param name="notificationEvent">The event that occurred requiring notification</param>
	/// <param name="template">The template to render</param>
	/// <param name="group"></param>
	/// <param name="templateTransformerAsync">
	/// An async function which accepts a <see cref="TTemplateData"/> instance and returns a
	/// <see cref="TTemplateData"/> instance. Subclasses of
	/// <see cref="BaseNotificationTemplateData{TGroup}"/>
	/// will specify additional properties needed for
	/// template population. This method gives the caller a per-template-data-instance function
	/// that can populate those additional properties with data in the caller's scope
	/// </param>
	/// <param name="shouldLog"></param>
	/// <param name="cancellationToken"></param>
	private async Task NotifyAsync<TTemplateData>(
		IEnumerable<UserTemplateDto<TTemplateData, TGroup>> userTemplateDtos, string notificationEvent,
		INotificationTemplate template, TGroup group,
		Func<TTemplateData, Task<TTemplateData>> templateTransformerAsync,
		bool shouldLog,
		CancellationToken cancellationToken = default)
		where TTemplateData : BaseNotificationTemplateData<TGroup>, new()
	{
		var templateData = await SendNotifications(notificationEvent, userTemplateDtos, group, template,
			templateTransformerAsync);
		if (shouldLog)
			await LogNotifications(notificationEvent, templateData, group?.Id, cancellationToken);
	}

	/// <summary>
	/// Send the notification via the transport
	/// </summary>
	/// <param name="notificationEvent">The event that occurred requiring notification</param>
	/// <param name="userTemplateDtos">The users to notify</param>
	/// <param name="group">The group context in which templates can be populated</param>
	/// <param name="template">The template to be populated</param>
	/// <param name="templateTransformerAsync">
	/// An async function which accepts a <see cref="TTemplateData"/> instance and returns a
	/// <see cref="TTemplateData"/> instance. Subclasses of
	/// <see cref="BaseNotificationTemplateData{TGroup}"/>
	/// will specify additional properties needed for
	/// template population. This method gives the caller a per-template-data-instance function
	/// that can populate those additional properties with data in the caller's scope
	/// </param>
	private async Task<List<UserTemplateDto<TTemplateData, TGroup>>> SendNotifications<TTemplateData>(
		string notificationEvent, IEnumerable<UserTemplateDto<TTemplateData, TGroup>> userTemplateDtos,
		TGroup group, INotificationTemplate template,
		Func<TTemplateData, Task<TTemplateData>> templateTransformerAsync)
		where TTemplateData : BaseNotificationTemplateData<TGroup>, new()
	{
		var notifications = await Task.WhenAll(userTemplateDtos.Select(async u =>
		{
			var templateData = new TTemplateData
			{
				Group = group,
				NotificationEvent = notificationEvent,
				User = u.User,
				BaseUrl = ShardDomainConfiguration.Instance.BaseUrlWithShardKey(_shardKeyProvider.GetShardKey(),
					true)
			};
			if (templateTransformerAsync != null)
				templateData = await templateTransformerAsync(templateData);
			return new UserTemplateDto<TTemplateData, TGroup> { User = u.User, DisableEmail = u.DisableEmail, TemplateData = templateData };
		}));
		var renderableTemplate = (INotificationTemplate<TTemplateData, TGroup>)template;
		var addressedNotifications = notifications.Where(n => !n.DisableEmail)
			.ToDictionary(n => _destinationSelector(n.User), n => renderableTemplate.Render(n.TemplateData));
		await _transport.SendAsync(addressedNotifications);
		return notifications.ToList();
	}

	/// <summary>
	/// Persist a log of a notification having happened
	/// </summary>
	/// <param name="notificationEvent">The event that occurred requiring notification</param>
	/// <param name="groupId">The group context in which the event occurred</param>
	/// <param name="userTemplateDtos">The per-user template data that was sent</param>
	/// <param name="cancellationToken"></param>
	/// <returns></returns>
	private async Task LogNotifications<TTemplateData>(string notificationEvent,
		IReadOnlyList<UserTemplateDto<TTemplateData, TGroup>> userTemplateDtos, int? groupId,
		CancellationToken cancellationToken = default)
		where TTemplateData : BaseNotificationTemplateData<TGroup>, new()
	{
		var notificationLogs = await Task.WhenAll(userTemplateDtos.Select(async (td, index) =>
		{
			var notificationLog = (TNotificationLog)Activator.CreateInstance(typeof(TNotificationLog), groupId,
				td.User.Id, notificationEvent, td.DisableEmail);
			if (notificationLog == null) throw new NullReferenceException();
			await notificationLog.PopulateAdditionalDataAsync(userTemplateDtos[index].TemplateData);
			return notificationLog;
		}));
		await _dataAccess.PersistNotificationLogsAsync(notificationLogs, cancellationToken);
	}
}

#endregion PrivateImplementation