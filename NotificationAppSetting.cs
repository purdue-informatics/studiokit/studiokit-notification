﻿namespace StudioKit.Notification;

public class NotificationAppSetting
{
	public const string AzureCommunicationServiceConnectionString = "AzureCommunicationServiceConnectionString";

	public const string WhitelistEmailAddresses = "WhitelistEmailAddresses";

	public const string FromAddress = "FromAddress";
}