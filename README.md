# StudioKit.Notification

## About

This library contains functions for:
- Maintaining sets of templates for generating notifications to individual users & groups of users
- Generating (rendering) notifications from these templates
- Providing data to the templates
- Sending the notifications via email

Features for sending via SMS and in-app notifications are planned.

## Setup

### Classes To Implement

#### `NotificationEvent`

Holds the set of notification events (events that will occur in your application that require notification)
Static class. It will define one or more public const strings for each event in your application.

```csharp
		public const string GroupAssignmentStarted = "GroupAssignmentStarted";
```

#### `XXXNotificationTemplateData`

You'll implement subclasses of `BaseNotificationTemplateData`. Each is a
[POCO](https://en.wikipedia.org/wiki/Plain_old_CLR_object) that implements properties for data
you'll need (beyond what's provided in `BaseNotificationTemplateData`) that you'll need in your
templates.

```csharp
	public class GroupAssignmentNotificationTemplateData : BaseNotificationTemplateData<Group>
	{
		public GroupAssignment GroupAssignment { get; set; }
	}
```

#### `NotificationTemplateConfiguration`

You'll implement a subclass of `BaseNotificationTemplateConfiguration`.

Create a static method that populates `EventTemplates`

- Each key in that dictionary will be a `NotificationEvent`
- Each value will be a `TargetetdTemplates` instance which defines a `render` function and list of
  activities for either the `GroupTemplate` property or the `UserTemplate` property (or both)
  depending on if you'll be notifying a single user or a group (or both) with when this event occurs
- The `render` function is a function that accepts a `TemplateData` instance (i.e. the data you'll
  need for populating that template - the user, the group, etc) and returns an instance of a
  `NotificationMessage`. `NotificationMessage` is a plaintext and HTML representation of the
  notification as well as a subject and any attachment names. plaintext and HTML are recommended for
  email notifications because spam filters will deprioritize HTML-only email. Attachments is a
  dictionary of attachment names (`String`s) and `System.IO.Stream` instances. The html
  representation of your templates can reference these attachments in an `<img>` take like:

  ```html
	  <img src='cid:Images.purdue_logo.png' />
  ```

	Where `Images.purdue_logo.png` is the key in the dictionary of attachment names

	```csharp
	public class ApplicationNotificationTemplateConfiguration : BaseNotificationTemplateConfiguration
	{
		static ApplicationNotificationTemplateConfiguration()
		{
			EventTemplates = new Dictionary<string, TargetedTemplates>
			{
				{
					NotificationEvent.GroupAssignmentStarted,
					new TargetedTemplates
					{
						GroupTemplate = new NotificationTemplate<GroupAssignmentNotificationTemplateData, Group>(
							td =>
							{
								return new NotificationMessage(
									$"plain text notification that might include {td.User.FirstName}",
									$"<div>HTML notificaton that might include {td.Group.Name} and a logo via <a href='cid:Images.purdue_logo.png'> </div>",
									"Event occurred"
									new Dictionary<string, Func<Stream>>{ { attachment1.png, /* a stream on a resource */	} });
		                    },
							new List<string> {ApplicationActivity.GroupAssignmentSubmissionCreate})
		            }
				},
		   ...
		   }
		}
	}

	```

#### `NotificationLog`

If needed, implement a subclass of `BaseNotificationLog`. This class is used to create a record in the database of every notification that goes out the door (and those that would have if the user chose not to be notified).

If you'll want to persist some or all properties of your subclasses of `BaseNotificationTemplateData` to the database, you can define those properties here. Then create an overridden implementation of `PopulateAdditionalDataAsync()` (being sure to call the base method). This override will populate the property or properties you define on this subclass.

```csharp
	public class NotificationLog : BaseNotificationLog<Group>
	{
		// This was implemented because LINQ-to-Entities requires an empty constructor when it has to create a projection (i.e. .Select()),
		// even if you are only populating certain properties.
		public NotificationLog()
		{
		}

		public NotificationLog(int groupId, string userId, string notificationEvent, bool wasNotificationRefused) : base(groupId, userId, notificationEvent, wasNotificationRefused)
		{
		}

		public override async Task PopulateAdditionalDataAsync(BaseNotificationTemplateData<Group> templateData)
		{
			await base.PopulateAdditionalDataAsync(templateData);
			if (templateData is GroupAssignmentNotificationTemplateData applicationTemplateData && applicationTemplateData.GroupAssignment != null)
				GroupAssignmentId = applicationTemplateData.GroupAssignment.Id;
			else if (templateData is LateActivityPermitNotificationTemplateData latePermitTemplateData &&
					 latePermitTemplateData.GroupAssignment != null)
				GroupAssignmentId = latePermitTemplateData.GroupAssignment.Id;
		}

		public int? GroupAssignmentId { get; private set; }
	}
```



## Configuration You'll Need

In order to allow users to configure whether to receieve notifications, you'll need to implement a `UserSettingService` and related endpoints and business models. You'll want to use `StudioKit.Notification.DisableEmailSetting` as the type when persisting these settings.

---

Your application's `DbContext` will need:
1. to implement `INotificationDbContext`, i.e.:
	```csharp
		public class ApplicationDbContext : BaseDbContext<User, Group, Configuration>,
			INotificationDbContext<NotificationLog, User, Group, RoleActivity, Activity, GroupUserRole>
		{
			...
		}

	```
1. a `DbSet` for email notificaton preferences:
	```csharp
			public virtual DbSet<DisableEmailSetting> DisableEmailSettings { get; set; }
	```

---

In your application or webjob startup, you'll need to hook all of the above together, by registering the following in your DI container (i.e. autofac)

1. your `DbContext` as an `INotificationDbContext`
   ```csharp
	builder.RegisterType<ApplicationDbContext>()
		/* all your other registrations */
		.As<INotificationDbContext<NotificationLog, User, Group, RoleActivity, Activity, GroupUserRole>>()
		.InstancePerRequest();
   ```
1. the possible transports (email, SMS, etc). As of this writing, only email is available
   ```csharp
		builder.Register(ctx => EmailTransport.EmailClientFactory());

		builder.RegisterType<EmailTransport>()
			.As<INotificationTransport>();
   ```
1. configuration to tell `NotificationDataAccess` about your `DbContext`. `NotificationDataAccess` has functions that generate `IQueryable` instances to manage notification logging.
   ```csharp
	builder.RegisterType<NotificationDataAccess<NotificationLog, User, Group, RoleActivity, Activity, GroupUserRole>>()
		.UsingConstructor(typeof(INotificationDbContext<NotificationLog, User, Group, RoleActivity, Activity, GroupUserRole>));

   ```
1. configuration to tell `NotificationService` how to connect to your application. `NotificationService` is what you'll use to actually send out notifications in your application (described in the next section).
   ```csharp
	builder.RegisterType<NotificationService<NotificationLog, User, Group, RoleActivity, Activity, GroupUserRole>>()
		.As<INotificationService<Group>>()
		.WithParameter(new TypedParameter(typeof(NotificationTemplates), NotificationTemplates.GetInstance()))
		.WithParameter(new TypedParameter(typeof(Func<IUser, string>), new Func<IUser, string>(u => u.Email)));

   ```


## Notifying Users

This is the easy part!

Whether notifying users or groups, you'll provide the following

- the event
- the principal generating the notification (currently restricted to `SystemPrincipal`)
- (optional) a function that provides additional data for the notification
  - This is a function so it can close over values defined in the current call context. The type of
    the object passed to the function will have been defined in your template definiton for the
    event you specify here. And you'll specify it in the type parameter to the notification
    functions below. This function will return a task (so the notification service can populate the
    data asynchronously)

### Notifying an individual user


#### `NotifyUserAsync()`

You will also provide:

- the user to notify
- (optional) the id of the group to provide the context in which this notification is being generated

```csharp
await _notificationService.NotifyUserAsync<GroupAssignmentNotificationTemplateData>(
	notificationEvent, user, new SystemPrincipal(),
	groupAssignment.GroupId, data =>
	{
		data.GroupAssignment = groupAssignment;
		return Task.FromResult(data);
	}, cancellationToken);

```

### Notifying a group of users

#### `NotifyGroupAsync()`

You will also provide:

- The id of the group to notify
- (optional) A function that accepts an `IQueryable<Group>` and returns an `IQueryable<Group>`. This
  allows you to filter the users to which the notification will be sent
- (optional, defaults to `true`) a boolean to specify whether the notifications are persisted to the `NotificationLog` table. This would normally not be overriden except in cases where the `NotificationLog` entries have to be manually generated in webjobs or other concurrency-sensitive scenarios

```csharp
await _notificationService.NotifyGroupAsync(groupAssignment.GroupId,
	NotificationEvent.GroupAssignmentDatesChanged, new SystemPrincipal(),
	(GroupAssignmentNotificationTemplateData data) =>
	{
		data.GroupAssignment = groupAssignment;
		return Task.FromResult(data);
	}, cancellationToken: cancellationToken);
```

All of the group members (except those with notifications disabled) will be sent a notification with
data populated accordingly. All notifications will be logged, including those that would have gone
to users that have notifications disabled (the log will reflect this preference).
