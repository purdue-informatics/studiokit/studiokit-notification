﻿using StudioKit.Data;
using StudioKit.Data.Entity.Extensions;
using StudioKit.Notification.BusinessLogic.Models;
using StudioKit.Notification.Models.Interfaces;
using System.Threading.Tasks;

namespace StudioKit.Notification.Models;

/// <summary>
/// This model exists to track notifications that get sent to users. Properties are read-only
/// in order to satisfy requirements for <see cref="GetHashCode"/>. Properties have private
/// setters in order for EF to set them when retrieving from the DB
/// </summary>
/// <typeparam name="TGroup">The type of group for associating with notifications</typeparam>
public class BaseNotificationLog<TGroup> : ModelBase
	where TGroup : IGroup
{
	public BaseNotificationLog()
	{
	}

	public BaseNotificationLog(int groupId, string userId, string notificationEvent, bool wasNotificationRefused)
	{
		GroupId = groupId;
		UserId = userId;
		NotificationEvent = notificationEvent;
		WasNotificationRefused = wasNotificationRefused;
	}

	public int? GroupId { get; private set; }
	public string UserId { get; private set; }
	public string NotificationEvent { get; private set; }

	// Did the user have notifications disabled at the time it was sent
	public bool WasNotificationRefused { get; private set; }

	#region Equality Comparison

	public virtual Task PopulateAdditionalDataAsync(BaseNotificationTemplateData<TGroup> templateData)
	{
		return Task.CompletedTask;
	}

	protected bool Equals(BaseNotificationLog<TGroup> other)
	{
		return GroupId == other.GroupId && UserId == other.UserId && NotificationEvent == other.NotificationEvent;
	}

	public override bool Equals(object obj)
	{
		if (ReferenceEquals(null, obj)) return false;
		if (ReferenceEquals(this, obj)) return true;
		if (!obj.GetType().EntityTypeEquals(GetType())) return false;
		return Equals((BaseNotificationLog<TGroup>)obj);
	}

	public override int GetHashCode()
	{
		unchecked
		{
			// ReSharper disable NonReadonlyMemberInGetHashCode
			// While not technically read only, the private set in only used when retrieving
			// from the DB via EF
			var hashCode = GroupId != null ? GroupId.GetHashCode() : 0;
			hashCode = (hashCode * 397) ^ (UserId != null ? UserId.GetHashCode() : 0);
			hashCode = (hashCode * 397) ^ (NotificationEvent != null ? NotificationEvent.GetHashCode() : 0);
			// ReSharper restore NonReadonlyMemberInGetHashCode
			return hashCode;
		}
	}

	#endregion Equality Comparison
}