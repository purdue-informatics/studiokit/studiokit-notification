﻿namespace StudioKit.Notification.Models.Interfaces;

public interface IActivity
{
	string Name { get; set; }
}