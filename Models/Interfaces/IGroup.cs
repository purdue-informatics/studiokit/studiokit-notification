﻿namespace StudioKit.Notification.Models.Interfaces;

/// <summary>
/// This project intends to pass around Groups, but doesn't access any properties on them.
/// This interface is just to get a type guard from passing other entities that satisfy
/// the ModelBase type requirement.
/// </summary>
public interface IGroup
{
}