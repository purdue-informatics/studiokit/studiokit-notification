﻿namespace StudioKit.Notification.Models.Interfaces;

public interface IRoleActivity<TActivity>
	where TActivity : IActivity
{
	string RoleId { get; set; }

	TActivity Activity { get; set; }
}