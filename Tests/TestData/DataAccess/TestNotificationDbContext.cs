﻿using Microsoft.EntityFrameworkCore;
using StudioKit.Notification.BusinessLogic.Models;
using StudioKit.Notification.DataAccess.Interfaces;
using StudioKit.Notification.Tests.TestData.Models;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Tests.TestData.DataAccess;
using StudioKit.Scaffolding.Tests.TestData.Models;
using StudioKit.Security.BusinessLogic.Interfaces;
using StudioKit.Utilities.Interfaces;
using System.Data.Common;

namespace StudioKit.Notification.Tests.TestData.DataAccess;

/// <summary>
/// Implementation of <see cref="DbContext"/> for testing notification-related services
/// </summary>
public class TestNotificationDbContext : TestBaseDbContext,
	INotificationDbContext<TestNotificationLog, TestBaseUser, TestBaseGroup, RoleActivity, Activity, GroupUserRole>

{
	public TestNotificationDbContext()
	{
	}

	public TestNotificationDbContext(DbContextOptions options, IDateTimeProvider dateTimeProvider, IPrincipalProvider principalProvider) :
		base(options, dateTimeProvider, principalProvider)
	{
	}

	public TestNotificationDbContext(DbConnection existingConnection, IDateTimeProvider dateTimeProvider,
		IPrincipalProvider principalProvider) :
		base(existingConnection, dateTimeProvider, principalProvider)
	{
	}

	protected override void OnModelCreating(ModelBuilder modelBuilder)
	{
		base.OnModelCreating(modelBuilder);

		modelBuilder.Entity<DisableEmailSetting>()
			.HasOne(s => (TestBaseUser)s.User);
	}

	public DbSet<DisableEmailSetting> DisableEmailSettings { get; set; }

	public DbSet<TestNotificationLog> NotificationLogs { get; set; }
}