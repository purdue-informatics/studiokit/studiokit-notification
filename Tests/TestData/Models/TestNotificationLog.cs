﻿using Microsoft.EntityFrameworkCore;
using StudioKit.Notification.Models;
using StudioKit.Scaffolding.Tests.TestData.Models;

namespace StudioKit.Notification.Tests.TestData.Models;

/// <summary>
/// Non-generic implementation of BaseNotificationLog for inclusion in a <see cref="DbContext"/>'s <see cref="DbSet"/>
/// </summary>
public class TestNotificationLog : BaseNotificationLog<TestBaseGroup>
{
	public TestNotificationLog()
	{
	}

	public TestNotificationLog(int groupId, string userId, string notificationEvent, bool wasNotificationRefused) : base(groupId, userId,
		notificationEvent, wasNotificationRefused)
	{
	}
}