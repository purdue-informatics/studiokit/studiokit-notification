﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSTestExtensions;
using StudioKit.Notification.BusinessLogic.Models;
using StudioKit.Notification.DataAccess;
using StudioKit.Notification.Tests.TestData.DataAccess;
using StudioKit.Notification.Tests.TestData.Models;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Tests.TestData.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudioKit.Notification.Tests.DataAccess;

/// <summary>
/// Tests for the data access tier of notification service
/// Note: This class has several "pass-through" methods that exist for the purpose
/// of separating data access from business logic. Since there is no logic in those
/// pass-through methods, this class does not contain tests that exercise those
/// methods
/// </summary>
[TestClass]
public class NotificationDataAccessTests : BaseTest
{
	private static INotificationTemplate _template;
	private static readonly NotificationTestDependencies Deps = new(NotificationTestDependencies.StartDate);
	private TestNotificationDbContext _dbContext;
	private IDbContextTransaction _transaction;
	private TestBaseGroup _group;

	[ClassInitialize]
	public static async Task ClassInitialize(TestContext testContext)
	{
		_template = new NotificationTemplate<TestNotificationTemplateData, TestBaseGroup>(_ => new NotificationMessage(), new List<string>
		{
			Deps.LearnerActivity.Name
		});

		// seed common data that will persist across tests
		await using var dbContext = Deps.PersistentDbContextFactory();
		await Deps.SeedRolesAndUsersAsync(dbContext);
		await Deps.SeedExternalProvidersAsync(dbContext);
		await Deps.SeedGroupWithDatesAsync(dbContext);
	}

	[TestInitialize]
	public void BeforeEach()
	{
		Deps.Reset(NotificationTestDependencies.StartDate);
		(_dbContext, _transaction) = Deps.PersistentDbContextAndTransactionFactory();
		_group = _dbContext.Groups.First();
	}

	[TestCleanup]
	public void AfterEach()
	{
		_transaction.Rollback();
		_transaction.Dispose();
		_dbContext.Dispose();
	}

	[ClassCleanup]
	public static void AfterAll()
	{
		Deps.Dispose();
	}

	#region GetUsersToNotifyQueryable

	[TestMethod]
	public async Task GetUsersToNotifyQueryable_ShouldReturnNoOneForNonexistentGroup()
	{
		var dataAccess = GetDataAccess();
		var usersCount = await dataAccess.GetUsersToNotifyQueryable<TestNotificationTemplateData>(2, _template).CountAsync();
		Assert.AreEqual(0, usersCount);
	}

	[TestMethod]
	public async Task GetUsersToNotifyQueryable_ShouldReturnAllGroupUsersButNotInstructors()
	{
		var dataAccess = GetDataAccess();
		var users = await dataAccess.GetUsersToNotifyQueryable<TestNotificationTemplateData>(_group.Id, _template).ToListAsync();
		Assert.AreEqual(2, users.Count);
		Assert.AreEqual(Deps.StudentId, users.First().User.Id);
		Assert.AreEqual(Deps.Student2Id, users.ElementAt(1).User.Id);
	}

	[TestMethod]
	public async Task GetUsersToNotifyQueryable_ShouldReturnAllGroupMembersWithNoTemplateActivitiesSpecified()
	{
		// template activities are null in this test case
		var template = new NotificationTemplate<TestNotificationTemplateData, TestBaseGroup>(_ => new NotificationMessage());
		var dataAccess = GetDataAccess();
		var users = await dataAccess.GetUsersToNotifyQueryable<TestNotificationTemplateData>(_group.Id, template).ToListAsync();
		Assert.AreEqual(4, users.Count);
		Assert.AreEqual(Deps.InstructorId, users.First().User.Id);
		Assert.AreEqual(Deps.GraderId, users.ElementAt(1).User.Id);
		Assert.AreEqual(Deps.StudentId, users.ElementAt(2).User.Id);
		Assert.AreEqual(Deps.Student2Id, users.ElementAt(3).User.Id);
	}

	#endregion GetUsersToNotifyQueryable

	#region HasEmailDisabledAsync

	[TestMethod]
	public async Task HasEmailDisabledAsync_ShouldBeFalseForNonDisabled()
	{
		var dataAccess = GetDataAccess();
		var disabled = await dataAccess.HasEmailDisabledAsync(Deps.Student);
		Assert.AreEqual(false, disabled);
	}

	[TestMethod]
	public async Task HasEmailDisabledAsync_ShouldBeTrueForDisabled()
	{
		_dbContext.DisableEmailSettings.Add(new DisableEmailSetting { UserId = Deps.Student2Id });
		await _dbContext.SaveChangesAsync();

		var dataAccess = GetDataAccess();
		var disabled = await dataAccess.HasEmailDisabledAsync(Deps.Student2);
		Assert.AreEqual(true, disabled);
	}

	#endregion HasEmailDisabledAsync

	#region Private Methods

	private NotificationDataAccess<TestNotificationLog, TestBaseUser, TestBaseGroup, RoleActivity, Activity, GroupUserRole> GetDataAccess()
	{
		_dbContext.ChangeTracker.Clear();
		return new NotificationDataAccess<TestNotificationLog, TestBaseUser, TestBaseGroup, RoleActivity, Activity, GroupUserRole>(
			_dbContext);
	}

	#endregion
}