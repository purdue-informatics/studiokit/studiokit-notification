﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudioKit.Configuration;
using StudioKit.Encryption;
using StudioKit.Notification.BusinessLogic.Models;
using StudioKit.Notification.BusinessLogic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudioKit.Notification.Tests;

[TestClass]
public class EmailTransportTests
{
	[ClassInitialize]
	public static void BeforeAll(TestContext testContext)
	{
		EncryptedConfigurationManager.Configuration = new ConfigurationBuilder()
			.AddInMemoryCollection(new Dictionary<string, string>
			{
				{ "FromAddress", "notifications@peercircuit.org" }
			}).Build();
	}

	[TestMethod]
	public async Task CreateEmailMessage_ShouldThrowWithoutBody()
	{
		var ex = await Assert.ThrowsExceptionAsync<ArgumentException>(async () =>
		{
			await EmailTransport.CreateEmailMessageAsync(new NotificationMessage(), new List<string>(), Tier.Local, new List<string>());
		});
		Assert.AreEqual("Trying to send an email with neither an html nor a plaintext body", ex.Message);
	}

	[TestMethod]
	public async Task CreateEmailMessageAsync_SendersShouldBeWhitelisted()
	{
		var whiteListedRecipients = new List<string> { "jase@purdue.edu" };
		var recipients = new List<string> { "jase@purdue.edu", "student@purdue.edu" };
		var body = new NotificationMessage( "Test", subject: "subject");
		var message = await EmailTransport.CreateEmailMessageAsync(body,
			recipients, Tier.Local, whiteListedRecipients);
		Assert.AreEqual("jase@purdue.edu", message.Recipients.To.Single().Address);
	}

	[TestMethod]
	public async Task CreateEmailMessageAsync_SendWithPlainTextOnly()
	{
		var recipients = new List<string> { "jase@purdue.edu" };
		var body = new NotificationMessage("Test plaintext body", subject: "subject");
		var message = await EmailTransport.CreateEmailMessageAsync(body, recipients);
		Assert.AreEqual(body.Plaintext, message.Content.PlainText);
	}

	[TestMethod]
	public async Task CreateEmailMessageAsync_SendWithHtmlOnly()
	{
		var recipients = new List<string> { "jase@purdue.edu" };
		var body = new NotificationMessage("", "<blink>Test HTML body</blink>", "subject");
		var message = await EmailTransport.CreateEmailMessageAsync(body, recipients);
		Assert.AreEqual(body.Html, message.Content.Html);
	}

	[TestMethod]
	public async Task CreateEmailMessageAsync_SendWithPlaintextAndHtml()
	{
		var recipients = new List<string> { "jase@purdue.edu" };
		var body = new NotificationMessage("Test plaintext body", "<blink>Test HTML body</blink>", "subject");
		var message = await EmailTransport.CreateEmailMessageAsync(body, recipients);
		Assert.AreEqual(body.Plaintext, message.Content.PlainText);
		Assert.AreEqual(body.Html, message.Content.Html);
	}
}