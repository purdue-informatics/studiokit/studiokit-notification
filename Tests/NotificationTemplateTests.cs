﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudioKit.Notification.BusinessLogic.Models;
using StudioKit.Scaffolding.Tests.TestData.Models;

namespace StudioKit.Notification.Tests;

[TestClass]
public class NotificationTemplateTests
{
	[TestMethod]
	public void AddTemplate_CanAddTemplate()
	{
		var templateToAdd = new NotificationTemplate<TestNotificationTemplateData, TestBaseGroup>(
			td => new NotificationMessage
			($"Hello {td.Greeting}!",
				$"<body><blink>Hello {td.Greeting}!</blink></body>"
			));

		var notificationTemplates = NotificationTemplates.GetInstance();
		notificationTemplates.AddOrReplaceTemplate(TestNotificationEvent.TestThingHappened, templateToAdd, NotificationTarget.User);

		var template =
			notificationTemplates.FindTemplate(TestNotificationEvent.TestThingHappened, NotificationTarget.User);

		Assert.AreEqual(templateToAdd, template);
	}

	[TestMethod]
	public void AddTemplate_CanAddUserAndGroupTemplates()
	{
		var userTemplateToAdd = new NotificationTemplate<TestNotificationTemplateData, TestBaseGroup>(
			td => new NotificationMessage
			($"User: Hello {td.Greeting}!",
				$"<body><blink>User: Hello {td.Greeting}!</blink></body>"
			));
		var groupTemplateToAdd = new NotificationTemplate<TestNotificationTemplateData, TestBaseGroup>(
			td => new NotificationMessage
			($"Group: Hello {td.Greeting}!",
				$"<body><blink>Group: Hello {td.Greeting}!</blink></body>"
			));

		var notificationTemplates = NotificationTemplates.GetInstance();
		notificationTemplates.AddOrReplaceTemplate(TestNotificationEvent.TestThingHappened, userTemplateToAdd, NotificationTarget.User);
		notificationTemplates.AddOrReplaceTemplate(TestNotificationEvent.TestThingHappened, groupTemplateToAdd, NotificationTarget.Group);

		var userTemplate =
			notificationTemplates.FindTemplate(TestNotificationEvent.TestThingHappened, NotificationTarget.User);
		Assert.AreEqual(userTemplateToAdd, userTemplate);
		var groupTemplate =
			notificationTemplates.FindTemplate(TestNotificationEvent.TestThingHappened, NotificationTarget.Group);
		Assert.AreEqual(groupTemplateToAdd, groupTemplate);
	}

	[TestMethod]
	public void AddTemplate_CanRemoveTemplate()
	{
		var templateToAdd = new NotificationTemplate<TestNotificationTemplateData, TestBaseGroup>(
			td => new NotificationMessage
			($"Hello {td.Greeting}!",
				$"<body><blink>Hello {td.Greeting}!</blink></body>"
			));

		var notificationTemplates = NotificationTemplates.GetInstance();
		notificationTemplates.AddOrReplaceTemplate(TestNotificationEvent.OtherTestThingThatHappened, templateToAdd,
			NotificationTarget.User);
		notificationTemplates.RemoveTemplate(TestNotificationEvent.OtherTestThingThatHappened);

		var template =
			notificationTemplates.FindTemplate(TestNotificationEvent.OtherTestThingThatHappened, NotificationTarget.User);

		Assert.IsNull(template);
	}

	[TestMethod]
	public void Render_ShouldRenderTemplate()
	{
		var template = new NotificationTemplate<TestNotificationTemplateData, TestBaseGroup>(
			td => new NotificationMessage($"Hello {td.Greeting}!",
				$"<body><blink>Hello {td.Greeting}!</blink></body>"));
		var result = template.Render(new TestNotificationTemplateData { Greeting = "world" });
		Assert.AreEqual("Hello world!", result.Plaintext);
		Assert.AreEqual("<body><blink>Hello world!</blink></body>", result.Html);
	}

	[TestMethod]
	public void Render_ShouldRenderWithOnlyPlaintext()
	{
		var template = new NotificationTemplate<TestNotificationTemplateData, TestBaseGroup>(
			td => new NotificationMessage($"Hello {td.Greeting}!"));
		var result = template.Render(new TestNotificationTemplateData { Greeting = "world" });
		Assert.AreEqual("Hello world!", result.Plaintext);
		Assert.AreEqual(null, result.Html);
	}

	[TestMethod]
	public void Render_ShouldRenderWithOnlyHtml()
	{
		var template = new NotificationTemplate<TestNotificationTemplateData, TestBaseGroup>(
			td => new NotificationMessage("", $"<body><blink>Hello {td.Greeting}!</blink></body>"));
		var result = template.Render(new TestNotificationTemplateData { Greeting = "world" });
		Assert.AreEqual("", result.Plaintext);
		Assert.AreEqual("<body><blink>Hello world!</blink></body>", result.Html);
	}
}