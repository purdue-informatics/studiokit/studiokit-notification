﻿namespace StudioKit.Notification.Tests;

internal class TestNotificationEvent
{
	public const string TestThingHappened = "TestThingHappened";
	public const string OtherTestThingThatHappened = "OtherTestThingThatHappened";
}