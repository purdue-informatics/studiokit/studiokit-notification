﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MSTestExtensions;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ErrorHandling.Interfaces;
using StudioKit.Notification.BusinessLogic.Interfaces;
using StudioKit.Notification.BusinessLogic.Models;
using StudioKit.Notification.BusinessLogic.Services;
using StudioKit.Notification.DataAccess;
using StudioKit.Notification.Properties;
using StudioKit.Notification.Tests.TestData.DataAccess;
using StudioKit.Notification.Tests.TestData.Models;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Tests.TestData.Models;
using StudioKit.Security.BusinessLogic.Models;
using StudioKit.Sharding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;

namespace StudioKit.Notification.Tests.BusinessLogic.Services;

[TestClass]
public class NotificationServiceTests : BaseTest
{
	private static readonly NotificationTestDependencies Deps = new(NotificationTestDependencies.StartDate);
	private TestNotificationDbContext _dbContext;
	private IDbContextTransaction _transaction;

	private TestBaseGroup _group;
	private NotificationTemplates _templates;
	private INotificationTransport _transport;
	private INotificationService<TestBaseGroup> _notificationService;

	#region Test Data

	private static readonly Func<TestNotificationTemplateData, NotificationMessage> TestNotificationUserBodyTemplate =
		td =>
			new NotificationMessage($"{td.Greeting.ToUpper()} {td.User.FirstName} {td.User.LastName}",
				$"<body>{td.Greeting.ToUpper()} {td.User.FirstName} {td.User.LastName}</body>");

	private static readonly Func<TestNotificationTemplateData, NotificationMessage> TestNotificationGroupBodyTemplate =
		td =>
			new NotificationMessage($"{td.Greeting.ToUpper()} {td.Group.Name}",
				$"<body>{td.Greeting.ToUpper()} {td.Group.Name}</body>");

	private static NotificationMessage TestUserRenderedNotificationMessage => new(
		$"HELLO {Deps.Student.FirstName} {Deps.Student.LastName}",
		$"<body>HELLO {Deps.Student.FirstName} {Deps.Student.LastName}</body>"
	);

	private static readonly NotificationMessage GroupRenderedNotificationMessage = new(
		"HELLO InitialGroupName",
		"<body>HELLO InitialGroupName</body>"
	);

	#endregion Test Data

	#region Callback Results

	private List<NotificationMessage> _notificationBodies = new();
	private List<string> _emailAddresses = new();
	private Exception _handledException;

	#endregion Callback Results

	#region Initialize

	[ClassInitialize]
	public static async Task ClassInitialize(TestContext testContext)
	{
		// seed common data that will persist across tests
		await using var dbContext = Deps.PersistentDbContextFactory();
		await Deps.SeedRolesAndUsersAsync(dbContext);
		await Deps.SeedExternalProvidersAsync(dbContext);
		await Deps.SeedGroupWithDatesAsync(dbContext);
	}

	[TestInitialize]
	public void BeforeEach()
	{
		Deps.Reset(NotificationTestDependencies.StartDate);
		(_dbContext, _transaction) = Deps.PersistentDbContextAndTransactionFactory();
		_group = _dbContext.Groups.Single();

		_handledException = null;
		ConfigureTemplates();
		ConfigureEmailTransport();
		ConfigureNotificationService();

		_dbContext.ChangeTracker.Clear();
	}

	[TestCleanup]
	public void AfterEach()
	{
		_transaction.Rollback();
		_transaction.Dispose();
		_dbContext.Dispose();
	}

	[ClassCleanup]
	public static void AfterAll()
	{
		Deps.Dispose();
	}

	private void ConfigureEmailTransport()
	{
		_notificationBodies = new List<NotificationMessage>();
		_emailAddresses = new List<string>();

		var emailTransportMock = new Mock<INotificationTransport>();
		emailTransportMock
			.Setup(m => m.SendAsync(It.IsAny<IDictionary<string, NotificationMessage>>()))
			.Callback((IDictionary<string, NotificationMessage> addressedNotifications) =>
			{
				_notificationBodies = addressedNotifications.Values.ToList();
				_emailAddresses = addressedNotifications.Keys.ToList();
			})
			.Returns(Task.CompletedTask);
		_transport = emailTransportMock.Object;
	}

	private void ConfigureTemplates()
	{
		_templates = NotificationTemplates.GetInstance();
		_templates.AddOrReplaceTemplate(TestNotificationEvent.TestThingHappened,
			new NotificationTemplate<TestNotificationTemplateData, TestBaseGroup>(
				TestNotificationUserBodyTemplate,
				new List<string> { Deps.LearnerActivityName }
			), NotificationTarget.User);
		_templates.AddOrReplaceTemplate(TestNotificationEvent.TestThingHappened,
			new NotificationTemplate<TestNotificationTemplateData, TestBaseGroup>(
				TestNotificationGroupBodyTemplate,
				new List<string> { Deps.LearnerActivityName }),
			NotificationTarget.Group);
	}

	private void ConfigureNotificationService()
	{
		string DestinationSelector(IUser u) => u.Email;
		var errorHandlerMock = new Mock<IErrorHandler>();
		errorHandlerMock.Setup(eh => eh.CaptureException(It.IsAny<Exception>(), It.IsAny<IExceptionUser>()))
			.Callback<Exception, IExceptionUser>((e, _) =>
			{
				_handledException = e;
			});
		errorHandlerMock.Setup(eh => eh.CaptureException(It.IsAny<Exception>()))
			.Callback<Exception>(e =>
			{
				_handledException = e;
			});
		var service =
			new NotificationService<TestNotificationLog, TestBaseUser, TestBaseGroup, RoleActivity, Activity,
				GroupUserRole>(
				new NotificationDataAccess<TestNotificationLog, TestBaseUser, TestBaseGroup, RoleActivity, Activity,
					GroupUserRole>(_dbContext),
				_transport,
				_templates,
				DestinationSelector,
				new SimpleShardKeyProvider("test"),
				errorHandlerMock.Object);
		_notificationService = service;
	}

	#endregion Initialize

	#region NotifyUserAsync

	[TestMethod]
	public void NotifyUserAsync_ShouldThrowWithoutSystemPrincipal()
	{
		Assert.ThrowsAsync<ForbiddenException>(Task.Run(async () =>
		{
			await _notificationService.NotifyUserAsync<TestNotificationTemplateData>(
				TestNotificationEvent.TestThingHappened,
				Deps.Student,
				new GenericPrincipal(new GenericIdentity("Test", "Test"), new[] { "OtherRole" }),
				_group.Id);
		}));
	}

	[TestMethod]
	public async Task NotifyUserAsync_ShouldLogErrorWithMissingTemplate()
	{
		await _notificationService.NotifyUserAsync<TestNotificationTemplateData>(
			TestNotificationEvent.OtherTestThingThatHappened,
			Deps.Student,
			new SystemPrincipal(),
			_group.Id);
		Assert.AreEqual(
			$"Template for event {TestNotificationEvent.OtherTestThingThatHappened} could not be found",
			_handledException.Message);
	}

	[TestMethod]
	public async Task NotifyUserAsync_ShouldNotifyRenderedMessages()
	{
		await _notificationService.NotifyUserAsync<TestNotificationTemplateData>(
			TestNotificationEvent.TestThingHappened,
			Deps.Student,
			new SystemPrincipal(),
			_group.Id, td =>
			{
				td.Greeting = "hello";
				return Task.FromResult(td);
			});

		Assert.IsNull(_handledException);

		Assert.AreEqual(1, _notificationBodies.Count);
		Assert.AreEqual(1, _emailAddresses.Count);
		Assert.AreEqual(Deps.Student.Email, _emailAddresses.First());
		Assert.AreEqual(TestUserRenderedNotificationMessage, _notificationBodies.First());
	}

	[TestMethod]
	public async Task NotifyUserAsync_ShouldNotifyRenderedMessagesToOnlyUser()
	{
		await _notificationService.NotifyUserAsync<TestNotificationTemplateData>(
			TestNotificationEvent.TestThingHappened,
			Deps.Student,
			new SystemPrincipal(),
			_group.Id,
			td =>
			{
				td.Greeting = "hello";
				return Task.FromResult(td);
			});

		Assert.IsNull(_handledException);

		Assert.AreEqual(1, _notificationBodies.Count);
		Assert.AreEqual(1, _emailAddresses.Count);
		Assert.AreEqual(Deps.Student.Email, _emailAddresses.First());
		Assert.AreEqual(TestUserRenderedNotificationMessage, _notificationBodies.First());
	}

	[TestMethod]
	public async Task NotifyUserAsync_ShouldNotNotifyWithDisableEmail()
	{
		_dbContext.DisableEmailSettings.Add(new DisableEmailSetting { UserId = Deps.StudentId });
		await _dbContext.SaveChangesAsync();

		_dbContext.ChangeTracker.Clear();

		await _notificationService.NotifyUserAsync<TestNotificationTemplateData>(
			TestNotificationEvent.TestThingHappened,
			Deps.Student,
			new SystemPrincipal(),
			_group.Id,
			td =>
			{
				td.Greeting = "hello";
				return Task.FromResult(td);
			});

		Assert.IsNull(_handledException);

		Assert.AreEqual(0, _notificationBodies.Count);
		Assert.AreEqual(0, _emailAddresses.Count);
	}

	[TestMethod]
	public async Task NotifyUserAsync_ShouldLogNotifications()
	{
		await _notificationService.NotifyUserAsync<TestNotificationTemplateData>(
			TestNotificationEvent.TestThingHappened,
			Deps.Student,
			new SystemPrincipal(),
			_group.Id,
			td =>
			{
				td.Greeting = "hello";
				return Task.FromResult(td);
			});

		Assert.IsNull(_handledException);

		var notificationLogs = await _dbContext.NotificationLogs.ToListAsync();
		Assert.AreEqual(1, notificationLogs.Count);
		Assert.AreEqual(new TestNotificationLog(_group.Id, Deps.StudentId, TestNotificationEvent.TestThingHappened, false),
			notificationLogs.First());
	}

	#endregion NotifyUserAsync

	#region NotifyGroupAsync

	[TestMethod]
	public void NotifyGroupAsync_ShouldThrowWithoutSystemPrincipal()
	{
		Assert.ThrowsAsync<ForbiddenException>(Task.Run(async () =>
		{
			await _notificationService.NotifyGroupAsync<TestNotificationTemplateData>(_group.Id,
				TestNotificationEvent.TestThingHappened,
				new GenericPrincipal(new GenericIdentity("Test", "Test"), new[] { "OtherRole" }));
		}));
	}

	[TestMethod]
	public async Task NotifyGroupAsync_ShouldLogErrorWithMissingTemplate()
	{
		await _notificationService.NotifyGroupAsync<TestNotificationTemplateData>(_group.Id,
			TestNotificationEvent.OtherTestThingThatHappened,
			new SystemPrincipal());
		Assert.AreEqual(string.Format(Strings.TemplateNotFound, TestNotificationEvent.OtherTestThingThatHappened),
			_handledException.Message);
	}

	[TestMethod]
	public async Task NotifyGroupAsync_ShouldNotifyRenderedMessages()
	{
		await _notificationService.NotifyGroupAsync<TestNotificationTemplateData>(_group.Id,
			TestNotificationEvent.TestThingHappened,
			new SystemPrincipal(),
			td =>
			{
				td.Greeting = "hello";
				return Task.FromResult(td);
			});

		Assert.IsNull(_handledException);

		Assert.AreEqual(2, _notificationBodies.Count);
		Assert.AreEqual(2, _emailAddresses.Count);
		Assert.AreEqual(Deps.Student.Email, _emailAddresses.First());
		Assert.AreEqual(GroupRenderedNotificationMessage, _notificationBodies.First());
	}

	[TestMethod]
	public async Task NotifyGroupAsync_ShouldLogNotifications()
	{
		await _notificationService.NotifyGroupAsync<TestNotificationTemplateData>(_group.Id,
			TestNotificationEvent.TestThingHappened,
			new SystemPrincipal(),
			td =>
			{
				td.Greeting = "hello";
				return Task.FromResult(td);
			});

		Assert.IsNull(_handledException);

		var notificationLogs = await _dbContext.NotificationLogs.ToListAsync();
		Assert.AreEqual(2, notificationLogs.Count);
		Assert.AreEqual(new TestNotificationLog(_group.Id, Deps.StudentId, TestNotificationEvent.TestThingHappened, false),
			notificationLogs.First());
	}

	[TestMethod]
	public async Task NotifyGroupAsync_ShouldLogNotificationsEvenWhenDisableEmail()
	{
		_dbContext.DisableEmailSettings.Add(new DisableEmailSetting { UserId = Deps.Student2Id });
		await _dbContext.SaveChangesAsync();

		_dbContext.ChangeTracker.Clear();

		await _notificationService.NotifyGroupAsync<TestNotificationTemplateData>(_group.Id,
			TestNotificationEvent.TestThingHappened,
			new SystemPrincipal(),
			td =>
			{
				td.Greeting = "hello";
				return Task.FromResult(td);
			});

		Assert.IsNull(_handledException);

		var notificationLogs = await _dbContext.NotificationLogs.ToListAsync();
		Assert.AreEqual(2, notificationLogs.Count);
		Assert.IsFalse(notificationLogs.Single(nl => nl.UserId == Deps.StudentId).WasNotificationRefused);
		Assert.IsTrue(notificationLogs.Single(nl => nl.UserId == Deps.Student2Id).WasNotificationRefused);
	}

	[TestMethod]
	public async Task NotifyGroupAsync_ShouldFilterUsers()
	{
		await _notificationService.NotifyGroupAsync<TestNotificationTemplateData>(_group.Id,
			TestNotificationEvent.TestThingHappened,
			new SystemPrincipal(),
			td =>
			{
				td.Greeting = "hello";
				return Task.FromResult(td);
			},
			users => users.Where(u => u.Id != Deps.Student2Id));

		Assert.IsNull(_handledException);

		Assert.AreEqual(1, _notificationBodies.Count);
		Assert.AreEqual(1, _emailAddresses.Count);
		Assert.AreEqual(Deps.Student.Email, _emailAddresses.First());
		Assert.AreEqual(GroupRenderedNotificationMessage, _notificationBodies.First());
	}

	#endregion NotifyGroupAsync
}