﻿using Microsoft.EntityFrameworkCore;
using StudioKit.Data.Entity.Identity;
using StudioKit.ExternalProvider.Models.Interfaces;
using StudioKit.Notification.Tests.TestData.DataAccess;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Tests;
using StudioKit.Scaffolding.Tests.TestData.Models;
using StudioKit.Security.BusinessLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Notification.Tests;

public class NotificationTestDependencies : BaseScaffoldingTestDependencies<TestNotificationDbContext, TestBaseUser, TestBaseGroup,
	BaseConfiguration>
{
	#region Readonly Properties

	public readonly string Student2Id = Guid.NewGuid().ToString();

	public readonly string LearnerActivityName = "CanLearnThings";
	public readonly string InstructorActivityName = "CanTeachThings";

	#endregion

	#region Instance Properties

	public Activity LearnerActivity;
	public Activity InstructorActivity;
	public TestBaseUser Student2;

	public IPrincipal Student2Principal { get; }

	public IPrincipalProvider Student2PrincipalProvider { get; }

	#endregion

	public NotificationTestDependencies(DateTime dateTime) : base(dateTime)
	{
		LearnerActivity = new Activity { Name = LearnerActivityName };
		InstructorActivity = new Activity { Name = InstructorActivityName };

		(Student2Principal, Student2PrincipalProvider) = GeneratePrincipalAndProvider(Student2Id);
	}

	#region DbConnection and DbContext

	protected override void LoadSeededEntities(TestNotificationDbContext dbContext)
	{
		base.LoadSeededEntities(dbContext);
		LearnerActivity = dbContext.Activities.Single(a => a.Name.Equals(LearnerActivityName));
		InstructorActivity = dbContext.Activities.Single(a => a.Name.Equals(InstructorActivityName));
		Student2 = dbContext.Users.Single(u => u.Id.Equals(Student2Id));
	}

	#endregion DbConnection and DbContext

	#region Seeding

	public override async Task SeedRolesAndUsersAsync(TestNotificationDbContext dbContext, CancellationToken cancellationToken = default)
	{
		await base.SeedRolesAndUsersAsync(dbContext, cancellationToken);

		var groupOwnerRole = await dbContext.Roles.SingleAsync(r => r.Name == BaseRole.GroupOwner, cancellationToken: cancellationToken);
		var groupLearnerRole =
			await dbContext.Roles.SingleAsync(r => r.Name == BaseRole.GroupLearner, cancellationToken: cancellationToken);
		dbContext.RoleActivities.Add(new RoleActivity { Role = groupOwnerRole, Activity = InstructorActivity });
		dbContext.RoleActivities.Add(new RoleActivity { Role = groupLearnerRole, Activity = LearnerActivity });

		Student2 = GenerateUser(Student2Id);
		dbContext.Users.Add(Student2);
		await dbContext.SaveChangesAsync(cancellationToken);
	}

	public override async Task SeedGroupUsersAsync(TestNotificationDbContext dbContext, int groupId,
		CancellationToken cancellationToken = default)
	{
		await base.SeedGroupUsersAsync(dbContext, groupId, cancellationToken);

		var groupLearnerRole = await dbContext.Roles
			.SingleAsync(r => r.Name.Equals(BaseRole.GroupLearner), cancellationToken);

		var groupUserRoles = new List<GroupUserRole>
		{
			new()
			{
				UserId = Student2Id,
				RoleId = groupLearnerRole.Id,
				GroupId = groupId
			}
		};
		var groupUserRoleLogs = groupUserRoles
			.Select(gur => new GroupUserRoleLog
			{
				UserId = gur.UserId,
				RoleId = gur.RoleId,
				GroupId = gur.GroupId,
				Type = GroupUserRoleLogType.Added
			})
			.ToList();
		dbContext.GroupUserRoles.AddRange(groupUserRoles);
		dbContext.GroupUserRoleLogs.AddRange(groupUserRoleLogs);
		await dbContext.SaveChangesAsync(cancellationToken);
	}

	#endregion
}