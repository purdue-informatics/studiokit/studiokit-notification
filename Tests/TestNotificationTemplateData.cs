﻿using StudioKit.Notification.BusinessLogic.Models;
using StudioKit.Scaffolding.Tests.TestData.Models;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]

namespace StudioKit.Notification.Tests;

internal class TestNotificationTemplateData : BaseNotificationTemplateData<TestBaseGroup>
{
	public string Greeting { get; set; }
}