﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using StudioKit.Data;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Notification.BusinessLogic.Models;
using StudioKit.Notification.Models;
using StudioKit.Notification.Models.Interfaces;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Notification.DataAccess.Interfaces;

/// <summary>
/// Interface for including notification-specific entities in a <see cref="DbContext"/>
/// </summary>
/// <typeparam name="TNotificationLog">The type used for logging notifications</typeparam>
/// <typeparam name="TUser">The type of IdentityUser</typeparam>
/// <typeparam name="TGroup">The type of Group</typeparam>
/// <typeparam name="TRoleActivity">The type of RoleActivity</typeparam>
/// <typeparam name="TActivity">The type of Activity</typeparam>
/// <typeparam name="TGroupUserRole">The type of GroupUserRole</typeparam>
public interface INotificationDbContext<TNotificationLog, TUser, TGroup, TRoleActivity, TActivity, TGroupUserRole>
	where TGroup : IGroup
	where TNotificationLog : BaseNotificationLog<TGroup>
	where TUser : IdentityUser, IUser
	where TRoleActivity : class, IRoleActivity<TActivity>
	where TActivity : class, IActivity
	where TGroupUserRole : class, IGroupUserRole
{
	Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);

	#region DbSets

	DbSet<TUser> Users { get; set; }

	DbSet<DisableEmailSetting> DisableEmailSettings { get; set; }

	DbSet<TNotificationLog> NotificationLogs { get; set; }

	DbSet<TRoleActivity> RoleActivities { get; set; }

	DbSet<TGroupUserRole> GroupUserRoles { get; set; }

	#endregion DbSets

	Task<TEntity> FindEntityAsync<TEntity>(int entityKey,
		bool throwIfSoftDeleted = true,
		string notFoundMessage = null,
		CancellationToken cancellationToken = default)
		where TEntity : ModelBase;
}