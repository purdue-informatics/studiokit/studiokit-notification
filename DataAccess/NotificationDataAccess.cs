﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using StudioKit.Data;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Notification.BusinessLogic.Models;
using StudioKit.Notification.DataAccess.Interfaces;
using StudioKit.Notification.Models;
using StudioKit.Notification.Models.Interfaces;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Notification.DataAccess;

/// <summary>
/// Data access methods for notifications
/// </summary>
/// <typeparam name="TNotificationLog">The type used to store the data about the notification for persistence</typeparam>
/// <typeparam name="TUser">The type of user for associating with notifications</typeparam>
/// <typeparam name="TGroup">The type of group for associating with notifications</typeparam>
/// <typeparam name="TRoleActivity">The type of RoleActivity</typeparam>
/// <typeparam name="TActivity">The type of Activity</typeparam>
/// <typeparam name="TGroupUserRole">The type of GroupUserRole</typeparam>
public class NotificationDataAccess<TNotificationLog, TUser, TGroup, TRoleActivity, TActivity, TGroupUserRole>
	where TNotificationLog : BaseNotificationLog<TGroup>
	where TUser : IdentityUser, IUser
	where TGroup : ModelBase, IGroup
	where TRoleActivity : class, IRoleActivity<TActivity>, new()
	where TActivity : class, IActivity, new()
	where TGroupUserRole : class, IGroupUserRole, new()
{
	private readonly INotificationDbContext<TNotificationLog, TUser, TGroup, TRoleActivity, TActivity, TGroupUserRole> _dbContext;

	/// <summary>
	/// Parameterless constructor to satisfy mocking requirements
	/// </summary>
	public NotificationDataAccess()
	{
	}

	public NotificationDataAccess(
		INotificationDbContext<TNotificationLog, TUser, TGroup, TRoleActivity, TActivity, TGroupUserRole> dbContext)
	{
		_dbContext = dbContext;
	}

	/// <summary>
	/// <para>Get users from a group that have any of the activities specified in the notification template, along with their `DisableEmail` setting.</para>
	/// <para>If the template does not specify any activities then it goes to all members of the group.</para>
	/// <para>Additional user filtering can be provided with <paramref name="userFilter"/>, e.g. assignment progress requirements.</para>
	/// </summary>
	/// <param name="groupId">The group in which to locate users</param>
	/// <param name="template">The template to examine for activities</param>
	/// <param name="userFilter">(Optional) Provide additional user filtering , e.g. assignment progress requirements.</param>
	/// <returns>A queryable of user DTOs, containing the users and their `DisableEmail` settings.</returns>
	/// <typeparam name="TTemplateData">The type of data used to populate the notifications</typeparam>
	public virtual IQueryable<UserTemplateDto<TTemplateData, TGroup>>
		GetUsersToNotifyQueryable<TTemplateData>(int groupId, INotificationTemplate template,
			Func<IQueryable<IUser>, IQueryable<IUser>> userFilter = null)
		where TTemplateData : BaseNotificationTemplateData<TGroup>, new()
	{
		var groupUserRolesQueryable = _dbContext.GroupUserRoles
			.Where(gur => gur.GroupId == groupId);

		// if any template activities are specified, filter to users that have at least one of the activities
		if (template.Activities != null && template.Activities.Any())
			groupUserRolesQueryable = groupUserRolesQueryable
				.Join(_dbContext.RoleActivities,
					gur => gur.RoleId,
					ra => ra.RoleId,
					(gur, ra) => new { GroupUserRole = gur, ActivityName = ra.Activity.Name })
				.Where(o => template.Activities.Contains(o.ActivityName))
				.Select(o => o.GroupUserRole);

		IQueryable<IUser> usersQueryable = groupUserRolesQueryable
			.Join(_dbContext.Users,
				gur => gur.UserId,
				u => u.Id,
				(gur, u) => u);

		// use additional user filter if provided
		if (userFilter != null)
		{
			usersQueryable = userFilter(usersQueryable);
		}

		// LEFT JOIN to load DisableEmail settings
		var userDtosQueryable = usersQueryable
			.SelectMany(
				u => _dbContext.DisableEmailSettings
					.Where(s => s.UserId == u.Id)
					.DefaultIfEmpty(),
				(u, s) => new
				{
					User = u,
					DisableEmail = s != null
				})
			.Select(o => new UserTemplateDto<TTemplateData, TGroup> { User = o.User, DisableEmail = o.DisableEmail })
			.Distinct();

		return userDtosQueryable;
	}

	/// <summary>
	/// Data access method for locating a group from a group id
	/// </summary>
	/// <param name="groupId">The id of the group to locate</param>
	/// <param name="cancellationToken"></param>
	/// <returns>The located group</returns>
	public virtual async Task<TGroup> FindGroupAsync(int groupId, CancellationToken cancellationToken = default)
	{
		return await _dbContext.FindEntityAsync<TGroup>(groupId, cancellationToken: cancellationToken);
	}

	/// <summary>
	/// Data access method for determining if the user has disabled notifications
	/// </summary>
	/// <param name="user">The user to query</param>
	/// <param name="cancellationToken"></param>
	/// <returns>Whether the user has opted to disable email notifications</returns>
	public virtual async Task<bool> HasEmailDisabledAsync(IUser user, CancellationToken cancellationToken = default)
	{
		return await _dbContext.DisableEmailSettings
			.SingleOrDefaultAsync(des => des.UserId == user.Id, cancellationToken) != null;
	}

	/// <summary>
	/// Data access method for persisting notification logs
	/// </summary>
	/// <param name="notificationLogs">The logs to persist</param>
	/// <param name="cancellationToken"></param>
	/// <returns></returns>
	public virtual async Task PersistNotificationLogsAsync(TNotificationLog[] notificationLogs,
		CancellationToken cancellationToken = default)
	{
		_dbContext.NotificationLogs.AddRange(notificationLogs);
		await _dbContext.SaveChangesAsync(cancellationToken);
	}
}